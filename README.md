# GameBuddy

GameBuddy is a matchmaking service for offline activities. Matchmaking can be found in video games, where people play against each other online. Also, in competitive and administered sports the opponents are automatically selected. However, there is no possibility for casual players to find opponents offline. GameBuddy aims to make it easy to find a suitable opponent or a team for any sport or game. Whether you play tennis, ice-hockey, surf-boarding, climbing or chess, you could use GameBuddy to find a friend or establish a team.

## Technology

Backend framework: __Django__

[Frontend](https://bitbucket.org/knygard/gamebuddy-frontend) framework: __AngularJS__ <br>


### Notes on install

In the newest distribuon of OS X (Mavericks) one might need to set some additional flags to install pillow Python package.

Install pillow by:
sudo ARCHFLAGS=-Wno-error=unused-command-line-argument-hard-error-in-future pip install pillow


