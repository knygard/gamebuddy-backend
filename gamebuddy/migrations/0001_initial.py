# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'MyPhoto'
        db.create_table(u'gamebuddy_myphoto', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('image', self.gf('django.db.models.fields.files.ImageField')(max_length=254)),
        ))
        db.send_create_signal(u'gamebuddy', ['MyPhoto'])

        # Adding model 'User'
        db.create_table(u'gamebuddy_user', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('password', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('last_login', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
            ('is_superuser', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('username', self.gf('django.db.models.fields.CharField')(unique=True, max_length=30)),
            ('first_name', self.gf('django.db.models.fields.CharField')(max_length=30, blank=True)),
            ('last_name', self.gf('django.db.models.fields.CharField')(max_length=30, blank=True)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75, blank=True)),
            ('is_staff', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('is_active', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('date_joined', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
            ('gender', self.gf('django.db.models.fields.CharField')(max_length=1)),
            ('picture', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['gamebuddy.MyPhoto'], null=True, blank=True)),
        ))
        db.send_create_signal(u'gamebuddy', ['User'])

        # Adding M2M table for field groups on 'User'
        m2m_table_name = db.shorten_name(u'gamebuddy_user_groups')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('user', models.ForeignKey(orm[u'gamebuddy.user'], null=False)),
            ('group', models.ForeignKey(orm[u'auth.group'], null=False))
        ))
        db.create_unique(m2m_table_name, ['user_id', 'group_id'])

        # Adding M2M table for field user_permissions on 'User'
        m2m_table_name = db.shorten_name(u'gamebuddy_user_user_permissions')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('user', models.ForeignKey(orm[u'gamebuddy.user'], null=False)),
            ('permission', models.ForeignKey(orm[u'auth.permission'], null=False))
        ))
        db.create_unique(m2m_table_name, ['user_id', 'permission_id'])

        # Adding model 'ActivityType'
        db.create_table(u'gamebuddy_activitytype', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('description', self.gf('django.db.models.fields.TextField')(blank=True)),
        ))
        db.send_create_signal(u'gamebuddy', ['ActivityType'])

        # Adding model 'Activity'
        db.create_table(u'gamebuddy_activity', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=255)),
            ('activity_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['gamebuddy.ActivityType'])),
            ('user_created', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'gamebuddy', ['Activity'])

        # Adding model 'Place'
        db.create_table(u'gamebuddy_place', (
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255, primary_key=True)),
            ('description', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('user_created', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'gamebuddy', ['Place'])

        # Adding model 'Announcement'
        db.create_table(u'gamebuddy_announcement', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('creator', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['gamebuddy.User'])),
            ('activity_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['gamebuddy.ActivityType'])),
            ('activity', self.gf('django.db.models.fields.CharField')(max_length=127)),
            ('description', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('gender_preference', self.gf('django.db.models.fields.CharField')(max_length=1, blank=True)),
            ('skill', self.gf('django.db.models.fields.SmallIntegerField')(null=True, blank=True)),
            ('location', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['gamebuddy.Place'])),
            ('latitude', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=12, decimal_places=8, blank=True)),
            ('longitude', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=12, decimal_places=8, blank=True)),
            ('created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('modified', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'gamebuddy', ['Announcement'])

        # Adding model 'GamePlace'
        db.create_table(u'gamebuddy_gameplace', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('activity_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['gamebuddy.ActivityType'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=127)),
            ('address', self.gf('django.db.models.fields.CharField')(max_length=127)),
            ('business_hours', self.gf('django.db.models.fields.CharField')(max_length=127)),
            ('OwnerDescription', self.gf('django.db.models.fields.TextField')()),
            ('contact_info', self.gf('django.db.models.fields.CharField')(max_length=127)),
            ('link_to_reservation', self.gf('django.db.models.fields.URLField')(max_length=200)),
            ('link_to_homepage', self.gf('django.db.models.fields.URLField')(max_length=200)),
        ))
        db.send_create_signal(u'gamebuddy', ['GamePlace'])

        # Adding model 'UserReview'
        db.create_table(u'gamebuddy_userreview', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('creator', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['gamebuddy.User'])),
            ('game_place', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['gamebuddy.GamePlace'])),
            ('created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('review', self.gf('django.db.models.fields.TextField')()),
            ('stars', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal(u'gamebuddy', ['UserReview'])

        # Adding model 'GamePlaceImage'
        db.create_table(u'gamebuddy_gameplaceimage', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('game_place', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['gamebuddy.GamePlace'])),
            ('image', self.gf('django.db.models.fields.files.ImageField')(max_length=127, null=True, blank=True)),
            ('ImageDescription', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'gamebuddy', ['GamePlaceImage'])

        # Adding model 'Discussion'
        db.create_table(u'gamebuddy_discussion', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user1', self.gf('django.db.models.fields.related.ForeignKey')(related_name='user1', null=True, to=orm['gamebuddy.User'])),
            ('user2', self.gf('django.db.models.fields.related.ForeignKey')(related_name='user2', null=True, to=orm['gamebuddy.User'])),
            ('created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'gamebuddy', ['Discussion'])

        # Adding model 'Message'
        db.create_table(u'gamebuddy_message', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('sender', self.gf('django.db.models.fields.related.ForeignKey')(related_name='sender', null=True, to=orm['gamebuddy.User'])),
            ('receiver', self.gf('django.db.models.fields.related.ForeignKey')(related_name='receiver', null=True, to=orm['gamebuddy.User'])),
            ('discussion', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['gamebuddy.Discussion'])),
            ('created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('content', self.gf('django.db.models.fields.TextField')()),
            ('read', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'gamebuddy', ['Message'])


    def backwards(self, orm):
        # Deleting model 'MyPhoto'
        db.delete_table(u'gamebuddy_myphoto')

        # Deleting model 'User'
        db.delete_table(u'gamebuddy_user')

        # Removing M2M table for field groups on 'User'
        db.delete_table(db.shorten_name(u'gamebuddy_user_groups'))

        # Removing M2M table for field user_permissions on 'User'
        db.delete_table(db.shorten_name(u'gamebuddy_user_user_permissions'))

        # Deleting model 'ActivityType'
        db.delete_table(u'gamebuddy_activitytype')

        # Deleting model 'Activity'
        db.delete_table(u'gamebuddy_activity')

        # Deleting model 'Place'
        db.delete_table(u'gamebuddy_place')

        # Deleting model 'Announcement'
        db.delete_table(u'gamebuddy_announcement')

        # Deleting model 'GamePlace'
        db.delete_table(u'gamebuddy_gameplace')

        # Deleting model 'UserReview'
        db.delete_table(u'gamebuddy_userreview')

        # Deleting model 'GamePlaceImage'
        db.delete_table(u'gamebuddy_gameplaceimage')

        # Deleting model 'Discussion'
        db.delete_table(u'gamebuddy_discussion')

        # Deleting model 'Message'
        db.delete_table(u'gamebuddy_message')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'gamebuddy.activity': {
            'Meta': {'object_name': 'Activity'},
            'activity_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['gamebuddy.ActivityType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'}),
            'user_created': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'gamebuddy.activitytype': {
            'Meta': {'object_name': 'ActivityType'},
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'gamebuddy.announcement': {
            'Meta': {'object_name': 'Announcement'},
            'activity': ('django.db.models.fields.CharField', [], {'max_length': '127'}),
            'activity_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['gamebuddy.ActivityType']"}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'creator': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['gamebuddy.User']"}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'gender_preference': ('django.db.models.fields.CharField', [], {'max_length': '1', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'latitude': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '12', 'decimal_places': '8', 'blank': 'True'}),
            'location': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['gamebuddy.Place']"}),
            'longitude': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '12', 'decimal_places': '8', 'blank': 'True'}),
            'modified': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'auto_now_add': 'True', 'blank': 'True'}),
            'skill': ('django.db.models.fields.SmallIntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        u'gamebuddy.discussion': {
            'Meta': {'object_name': 'Discussion'},
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user1': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'user1'", 'null': 'True', 'to': u"orm['gamebuddy.User']"}),
            'user2': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'user2'", 'null': 'True', 'to': u"orm['gamebuddy.User']"})
        },
        u'gamebuddy.gameplace': {
            'Meta': {'object_name': 'GamePlace'},
            'OwnerDescription': ('django.db.models.fields.TextField', [], {}),
            'activity_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['gamebuddy.ActivityType']"}),
            'address': ('django.db.models.fields.CharField', [], {'max_length': '127'}),
            'business_hours': ('django.db.models.fields.CharField', [], {'max_length': '127'}),
            'contact_info': ('django.db.models.fields.CharField', [], {'max_length': '127'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'link_to_homepage': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'link_to_reservation': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '127'})
        },
        u'gamebuddy.gameplaceimage': {
            'ImageDescription': ('django.db.models.fields.TextField', [], {}),
            'Meta': {'object_name': 'GamePlaceImage'},
            'game_place': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['gamebuddy.GamePlace']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '127', 'null': 'True', 'blank': 'True'})
        },
        u'gamebuddy.message': {
            'Meta': {'object_name': 'Message'},
            'content': ('django.db.models.fields.TextField', [], {}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'discussion': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['gamebuddy.Discussion']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'read': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'receiver': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'receiver'", 'null': 'True', 'to': u"orm['gamebuddy.User']"}),
            'sender': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'sender'", 'null': 'True', 'to': u"orm['gamebuddy.User']"})
        },
        u'gamebuddy.myphoto': {
            'Meta': {'object_name': 'MyPhoto'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '254'})
        },
        u'gamebuddy.place': {
            'Meta': {'object_name': 'Place'},
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'primary_key': 'True'}),
            'user_created': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'gamebuddy.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'gender': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'picture': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['gamebuddy.MyPhoto']", 'null': 'True', 'blank': 'True'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'gamebuddy.userreview': {
            'Meta': {'object_name': 'UserReview'},
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'creator': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['gamebuddy.User']"}),
            'game_place': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['gamebuddy.GamePlace']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'review': ('django.db.models.fields.TextField', [], {}),
            'stars': ('django.db.models.fields.IntegerField', [], {})
        }
    }

    complete_apps = ['gamebuddy']