# -*- coding: utf-8 -*-

from django.db import models
from django.contrib.auth.models import AbstractUser

GENDER_CHOICES = (('m', 'male'), ('f', 'female'))
SKILL_CHOICES = ((1, 'novice'), (4, 'intermediate'), (7, 'advanced'), (10, 'pro'))

class MyPhoto(models.Model):
    image = models.ImageField(upload_to='photos', max_length=254)


class User(AbstractUser): # extends django's default User-model

    gender = models.CharField(max_length=1, choices=GENDER_CHOICES)
    picture = models.ForeignKey(MyPhoto, null=True, blank=True)

    def __unicode__(self):
        return self.username


class ActivityType(models.Model):
    '''
    Activity type is e.g. 'sports' or 'board games'
    '''
    name = models.CharField(max_length=255)
    description = models.TextField(blank=True)

    def __unicode__(self):
        return self.name


class Activity(models.Model):
    '''
    Activity, e.g. 'ice hockey' or 'badminton'
    '''
    name = models.CharField(max_length=255, unique=True)
    activity_type = models.ForeignKey(ActivityType)
    user_created = models.BooleanField(default=False)

    def __unicode__(self):
        return self.name


class Place(models.Model):

    name = models.CharField(max_length=255, primary_key=True)
    description = models.TextField(blank=True)
    user_created = models.BooleanField(default=False)

    def __unicode__(self):
        return self.name


class Announcement(models.Model):

    creator = models.ForeignKey(User)
    activity_type = models.ForeignKey(ActivityType)
    activity = models.CharField(max_length=127)
    description = models.TextField(blank=True)
    gender_preference = models.CharField(max_length=1, choices=GENDER_CHOICES, blank=True)
    skill = models.SmallIntegerField(null=True, blank=True, choices=SKILL_CHOICES)
    location = models.ForeignKey(Place)
    latitude = models.DecimalField(max_digits=12, decimal_places=8, blank=True, null=True)
    longitude = models.DecimalField(max_digits=12, decimal_places=8, blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True)
    modified  = models.DateTimeField(auto_now_add=True, auto_now=True)

    def __unicode__(self):
        return "%s: %s, %s" % (self.creator, self.activity_type, self.activity)

    @property
    def coordinates(self):
        return {'latitude': self.latitude, 'longitude': self.longitude}

    @property
    def skill_level(self):
        if (self.skill):
            name = dict(SKILL_CHOICES)[int(self.skill)]
        else:
            name = "any"
        return {'name': name, 'value': self.skill}

class GamePlace(models.Model):
    activity_type = models.ForeignKey(ActivityType)
    name = models.CharField(max_length=127, null=False)
    address = models.CharField(max_length=127, null=False)
    business_hours = models.CharField(max_length=127, null=False)
    OwnerDescription = review = models.TextField(blank=False)
    contact_info = models.CharField(max_length=127, null=False)
    link_to_reservation = models.URLField()
    link_to_homepage = models.URLField()


class UserReview(models.Model):
    creator = models.ForeignKey(User)
    game_place = models.ForeignKey(GamePlace)
    created = models.DateTimeField(auto_now_add=True)
    review = models.TextField(blank=False)
    stars = models.IntegerField()


class GamePlaceImage(models.Model):
    game_place = models.ForeignKey(GamePlace)
    image = models.ImageField(upload_to='images/game_place/', max_length=127, null=True, blank=True)
    ImageDescription = models.TextField(blank=False)


class Discussion(models.Model):
    user1 = models.ForeignKey(User, null=True, related_name='user1')
    user2 = models.ForeignKey(User, null=True, related_name='user2')
    created = models.DateTimeField(auto_now_add=True)


class Message(models.Model):
    sender = models.ForeignKey(User, null=True, related_name='sender')
    receiver = models.ForeignKey(User, null=True, related_name='receiver')
    discussion = models.ForeignKey(Discussion)
    created = models.DateTimeField(auto_now_add=True)
    content = models.TextField(blank=False)
    read = models.BooleanField(default=False)
