from django.conf.urls import patterns, include, url

from gamebuddy.API.ActivityTypeAPI import ActivityTypeAPI
from gamebuddy.API.ActivityAPI import ActivityAPI
from gamebuddy.API.UserAPI import UserAPI
from gamebuddy.API.LoadAPI import LoadAPI
from gamebuddy.API.LogoutAPI import LogoutAPI
from gamebuddy.API.AnnouncementAPI import AnnouncementAPI
from gamebuddy.API.UserPhotoAPI import PhotoList, PhotoDetail
from gamebuddy.API.MessageAPI import MessageAPI, DiscussionBetweenTwoUsersAPI, UsersDiscussions, GetAllDiscussions
from gamebuddy.API.PlaceAPI import PlaceAPI

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),

    # authentication
    url(r'^auth/login/?$', 'rest_framework.authtoken.views.obtain_auth_token', name="login"),
    url(r'^auth/load/?$', LoadAPI.as_view(), name="load"),
    url(r'^auth/logout/?$', LogoutAPI.as_view(), name="logout"),


    url(r'^activitytypes/(?P<id>[0-9]{1,9})/?$', ActivityTypeAPI.as_view()),
    url(r'^activitytypes/?$', ActivityTypeAPI.as_view()),
    url(r'^activitytypes/register', ActivityTypeAPI.as_view()),

    url(r'^activities/?$', ActivityAPI.as_view()),

    url(r'^users/(?P<id>[0-9]{1,9})/?$', UserAPI.as_view()),
    url(r'^users/?$', UserAPI.as_view()),
    url(r'^users/register',  UserAPI.as_view()),

    url(r'^announcements/(?P<id>[0-9]{1,9})/?$', AnnouncementAPI.as_view()),
    url(r'^announcements/?$', AnnouncementAPI.as_view()),
    url(r'^announcements/user/(?P<userId>[0-9]{1,9})/?$', AnnouncementAPI.as_view()),

    url(r'^api/photo/$', PhotoList.as_view(), name='myphoto-list'),
    url(r'^api/photo/(?P<pk>[0-9]+)/$', PhotoDetail.as_view()),

    url(r'^message/(?P<id>[0-9]{1,9})/?$', MessageAPI.as_view()),
    url(r'^messages/?$', MessageAPI.as_view()),
    url(r'^discussion/(?P<user1_id>[0-9]{1,9})/(?P<user2_id>[0-9]{1,9})/?$', DiscussionBetweenTwoUsersAPI.as_view()), # get single discussion between a and b
    url(r'^discussions/(?P<user>[0-9]{1,9})', UsersDiscussions.as_view()),
    url(r'^discussions/?$', GetAllDiscussions.as_view()),

    url(r'^places/(?P<name>.*)/?$', PlaceAPI.as_view()),
    url(r'^places/?$', PlaceAPI.as_view())
)
