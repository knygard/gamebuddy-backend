# -*- coding: utf-8 -*-

from rest_framework.views import APIView
from rest_framework.response import Response
# from rest_framework.permissions import IsAuthenticated
import rest_framework.exceptions as exc

from gamebuddy.exceptions import NotFound
from gamebuddy.models import ActivityType, Announcement
from gamebuddy.serializers import ActivityTypeSerializerGET, ActivityTypeSerializerPOST, AnnouncementSerializerGET


class ActivityTypeAPI(APIView):

    # permission_classes = (IsAuthenticated,)

    def get(self, request, id=None):
        if id:
            return self.get_single_type_and_its_announcements(request, id)

        return self.get_all_activity_types(request)

    def get_all_activity_types(self, request):

        activity_types = ActivityType.objects.all()
        if not activity_types:
            raise NotFound("No activity types could be found.")

        serializer = ActivityTypeSerializerGET(activity_types, many=True)

        return Response(serializer.data, 200)


    def get_single_type_and_its_announcements(self, request, id):

        try:
            activity_type_and_announcements = Announcement.objects.filter(activity_type=id)
        except Announcement.DoesNotExist:
            raise NotFound("Requested announcements cannot be found.")

        serializer = AnnouncementSerializerGET(activity_type_and_announcements)

        return Response(serializer.data, 200)

    def post(self, request):

        print request.DATA
        serializer = ActivityTypeSerializerPOST(data=request.DATA)
        if serializer.is_valid():
            serializer.save()
            get_serializer = ActivityTypeSerializerGET(serializer.object)
            print get_serializer.data
            return Response(get_serializer.data, 201)
        else:
            return Response(serializer.errors, 400)

        raise exc.ParseError("Activity type could not be created")
