# -*- coding: utf-8 -*-

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import exceptions as exc

from gamebuddy.serializers import UserSerializerGET


class LoadAPI(APIView):

    def get(self, request):
        if request.user.is_authenticated():
            serializer = UserSerializerGET(request.user)
            return Response(serializer.data, 200)
        else:
            return Response("User is not logged in.", 401)
