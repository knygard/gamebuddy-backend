# -*- coding: utf-8 -*-

from rest_framework.views import APIView
from rest_framework.response import Response
import rest_framework.exceptions as exc

from gamebuddy.exceptions import NotFound
from gamebuddy.models import Activity
from gamebuddy.serializers import ActivitySerializerGET


class ActivityAPI(APIView):

    def get(self, request):

        activities = Activity.objects.all().order_by('name')

        activity_type = request.GET.get('activity_type', None)
        if activity_type:
            activities = activities.filter(activity_type=activity_type)

        if not activities:
            raise NotFound("No activity types could be found.")

        serializer = ActivitySerializerGET(activities, many=True)

        return Response(serializer.data, 200)
