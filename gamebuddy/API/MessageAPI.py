# -*- coding: utf-8 -*-

from rest_framework.views import APIView
from rest_framework.response import Response
# from rest_framework.permissions import IsAuthenticated
import rest_framework.exceptions as exc
from django.http import HttpResponse
from gamebuddy.exceptions import NotFound
from gamebuddy.models import Message, Discussion
from gamebuddy.serializers import MessageGET, MessagePOST, DiscussionsGET, DiscussionPOST

from rest_framework.permissions import IsAuthenticated
from django.db.models import Q

class MessageAPI(APIView):

    #permission_classes = (IsAuthenticated)


    def get(self, request, id=None):
        if id:
            return self.get_single_message(request, id)

        return self.get_all_messages(request)

    def get_all_messages(self, request):

        messages = Message.objects.all()
        if not messages:
            raise NotFound("No messages could be found.")

        serializer = MessageGET(messages, many=True)

        return Response(serializer.data, 200)


    def get_single_message(self, request, id):

        try:
            messages = Message.objects.filter(id=id)
        except Message.DoesNotExist:
            raise NotFound("Requested message cannot be found.")

        serializer = MessageGET(messages)

        return Response(serializer.data, 200)

    def post(self, request):
       serializer = MessagePOST(data=request.DATA)
       if serializer.is_valid():
           serializer.save()

           return Response(serializer.data, 200)
       return Response(serializer.errors, 400)

    def put(self, request, id):
        try:
            message = Message.objects.filter(id=id)
        except Message.DoesNotExist:
            raise NotFound("Requested message cannot be found.")
        message.update(read=True)
        serializer = MessageGET(message)

        return Response(serializer.data, 200)

    def delete(self, request, id):
        message = Message.objects.filter(id=id)
        message.delete()
        return Response({'result':'deleted'}, 200)


class DiscussionBetweenTwoUsersAPI(APIView):

    def get(self, request, user1_id, user2_id):
      messages = Message.objects.filter(Q(sender=user1_id) | Q(sender=user2_id)).filter(Q(receiver=user2_id) | Q(receiver=user1_id))
      serializer = MessageGET(messages, many=True)
      return Response(serializer.data, 200)


class UsersDiscussions(APIView):

    def get(self, request, user):
      discussions = Discussion.objects.filter(Q(user1=user) | Q(user2=user))
      serializer = DiscussionsGET(discussions, many=True)
      return Response(serializer.data, 200)

class GetAllDiscussions(APIView):
    
    def get(self, request):
        discussions = Discussion.objects.all()
        if not discussions:
            raise NotFound("No discussions types could be found.")

        serializer = DiscussionsGET(discussions, many=True)

        return Response(serializer.data, 200)


    def post(self, request):
       discussions = Discussion.objects.filter(Q(user1=request.DATA['user1']) | Q(user2=request.DATA['user1'])).filter(Q(user1=request.DATA['user2']) | Q(user2=request.DATA['user2']))
       if (len(discussions) > 0):
          serializer = DiscussionsGET(discussions, many=True)
          return Response(serializer.data, 200)

       serializer = DiscussionPOST(data=request.DATA)
       if serializer.is_valid():
          serializer.save()

          return Response(serializer.data, 200)
       return Response(serializer.errors, 400)
