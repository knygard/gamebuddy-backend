# -*- coding: utf-8 -*-

from rest_framework.views import APIView
from rest_framework.response import Response
# from rest_framework.permissions import IsAuthenticated
import rest_framework.exceptions as exc
from django.http import HttpResponse

from gamebuddy.exceptions import NotFound
from gamebuddy.models import MyPhoto
from gamebuddy.serializers import PhotoSerializer



class PhotoList(APIView):
    

    def get(self, request, format=None):
        photo = MyPhoto.objects.all()
        if not photo:
            raise NotFound("No photo could be found.")

        serializer = PhotoSerializer(photo, many=True)
        return Response(serializer.data, 200)

    def post(self, request, format=None):
       serializer = PhotoSerializer(data=request.DATA, files=request.FILES)
       if serializer.is_valid():
           serializer.save()
           return Response(serializer.data, 200)
       return Response(serializer.errors, 400)


class PhotoDetail(APIView):


    def get_object(self, pk):
        try:
            return MyPhoto.objects.get(pk=pk)
        except MyPhoto.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        
        photo = self.get_object(pk)
        return HttpResponse(photo.image, mimetype="image/png")
        #serializer = PhotoSerializer(photo)
        #return Response(serializer.data)

    def put(self, request, pk, format=None):
        photo = self.get_object(pk)
        serializer = PhotoSerializer(photo, data=request.DATA, files=request.FILES)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        photo = self.get_object(pk)
        photo.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

        