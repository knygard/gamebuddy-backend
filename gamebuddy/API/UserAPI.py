# -*- coding: utf-8 -*-

from rest_framework.views import APIView
from rest_framework.response import Response
# from rest_framework.permissions import IsAuthenticated
import rest_framework.exceptions as exc

from gamebuddy.exceptions import NotFound
from gamebuddy.models import User
from gamebuddy.serializers import UserSerializerGET, UserSerializerPOST, PasswordSerializerPUT

from rest_framework.permissions import IsAuthenticated


class UserAPI(APIView):

    #permission_classes = (IsAuthenticated)

    def get(self, request, id=None):
        if id:
            return self.get_single(request, id)

        return self.get_many(request)

    def get_many(self, request):

        users = User.objects.all()
        if not users:
            raise NotFound("No users could be found.")

        serializer = UserSerializerGET(users, many=True)

        return Response(serializer.data, 200)


    def get_single(self, request, id):

        try:
            user = User.objects.get(id=id)
        except User.DoesNotExist:
            raise NotFound("Requested user cannot be found.")

        serializer = UserSerializerGET(user)

        return Response(serializer.data, 200)

    def post(self, request):
       serializer = UserSerializerPOST(data=request.DATA)
       if serializer.is_valid():
           serializer.save()
           print serializer.data

           return Response(serializer.data, 200)
       return Response(serializer.errors, 400)

    def put(self, request, id):


        serializer = PasswordSerializerPUT(data=request.DATA, partial=True)
        if serializer.is_valid():
            #print serializer.data
            user = User.objects.filter(id=id)
            # user.update(first_name=request.DATA['first_name'], last_name=request.DATA['last_name'], email=request.DATA['email'], password=request.DATA['password'], picture=request.DATA['picture'])
            if 'username' in request.DATA:
                user.update(username=request.DATA['username'])
            if 'first_name' in request.DATA:
                user.update(first_name=request.DATA['first_name'])
            if 'last_name' in request.DATA:
                user.update(last_name=request.DATA['last_name'])
            if 'email' in request.DATA:
                user.update(email=request.DATA['email'])
            if 'password' in request.DATA:
                user.update(password=serializer.data['password'])        
            if 'picture' in request.DATA:
                user.update(picture=request.DATA['picture'])  

            
            serializer = UserSerializerGET(user)
            return Response(serializer.data, 200)
        return Response(serializer.errors, 400)
        


   
