# -*- coding: utf-8 -*-

from rest_framework.views import APIView
from rest_framework.response import Response
import rest_framework.exceptions as exc

from gamebuddy.exceptions import NotFound
from gamebuddy.models import Announcement, ActivityType, Place
from gamebuddy.serializers import AnnouncementSerializerGET, AnnouncementSerializerPOST, AnnouncementSerializerDELETE

from django.core.paginator import Paginator


class AnnouncementAPI(APIView):

    # permission_classes = (IsAuthenticated,)

    def get(self, request, id=None, userId=None):

        if userId:
            return self.get_all_my_announcements(request, userId)

        elif id:
            return self.get_single(request, id)

        return self.get_many(request)

    def get_all_my_announcements(self, request, userId):
        try:
            announcement = Announcement.objects.filter(creator=userId)
        except Announcement.DoesNotExist:
            raise NotFound("Requested announcement cannot be found.")

        serializer = AnnouncementSerializerGET(announcement)

        return Response(serializer.data, 200)

    def get_many(self, request):
        countlimit = request.GET.get('countlimit', 300)
        objects_per_page = request.GET.get('objects_per_page', 15)
        page = request.GET.get('page', 1)
        activity_type = request.GET.get('activity_type', None)
        gender_preference = request.GET.get('gender_preference', None)
        skill = request.GET.get('skill', None)

        try:
            activity_type_object = ActivityType.objects.get(id=activity_type)
        except:
            activity_type_object = None

        announcements = Announcement.objects.all().order_by('-modified')

        if activity_type_object:
            announcements = announcements.filter(activity_type=activity_type_object.pk)
        if gender_preference in ['m', 'f']:
            announcements = announcements.filter(gender_preference=gender_preference)
        if skill:
            announcements = announcements.filter(skill=skill)

        announcements = announcements[:countlimit]



        p = Paginator(announcements, objects_per_page)
        serializer = AnnouncementSerializerGET(p.page(page), many=True)

        returnobj = {
            'data': serializer.data,
            'meta': {
                'objectcount': announcements.count()
            }
        }

        if not announcements:
            return Response(returnobj, 200)

        return Response(returnobj, 200)


    def get_single(self, request, id):

        try:
            announcement = Announcement.objects.get(id=id)
        except Announcement.DoesNotExist:
            raise NotFound("Requested announcement cannot be found.")

        serializer = AnnouncementSerializerGET(announcement)

        return Response(serializer.data, 200)


    def post(self, request):
        if not request.user.is_authenticated():
            return Response("User must be authenticated to create a new announcement", 401)

        # check if location exists
        location = request.DATA.get('location', None)
        if location:
            try: Place.objects.get(name=location)
            except Place.DoesNotExist:
                Place(name=location, user_created=True).save()

        request.DATA['creator'] = request.user.pk
        print request.DATA['creator']

        # print request.DATA
        serializer = AnnouncementSerializerPOST(data=request.DATA)
        if serializer.is_valid():
            serializer.save()
            get_serializer = AnnouncementSerializerGET(serializer.object)
            # print get_serializer.data
            return Response(get_serializer.data, 201)
        else:
            return Response(serializer.errors, 400)

        raise exc.ParseError("Announcement could not be created")

    def delete(self, request, id):
        announcement = Announcement.objects.get(id=id)
        serializer = AnnouncementSerializerDELETE(announcement)
        announcement.delete()
        return Response(serializer.data, 200)

    # def put(self, request, id=None):

        # TODO for updating announcement
