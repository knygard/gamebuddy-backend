# -*- coding: utf-8 -*-

from rest_framework.views import APIView
from rest_framework.response import Response
import rest_framework.exceptions as exc

from gamebuddy.exceptions import NotFound
from gamebuddy.models import Place
from gamebuddy.serializers import PlaceSerializer

from rest_framework.permissions import IsAuthenticated


class PlaceAPI(APIView):

    def get(self, request, name=None):
        if name:
            return self.get_single(request, name)

        return self.get_many(request)

    def get_single(self, request, name):
        try:
            place = Place.objects.get(name=name)
        except Place.DoesNotExist:
            return NotFound("Place could not be found.")

        serializer = PlaceSerializer(place)

        return Response(serializer.data, 200)

    def get_many(self, request):
        places = Place.objects.filter(user_created=False)
        if not places:
            raise NotFound("No places could be found.")

        serializer = PlaceSerializer(places, many=True)

        return Response(serializer.data, 200)
