from django.contrib import admin

from models import User, ActivityType, Activity, Announcement, Message, Discussion, \
                   Place


class UserAdmin(admin.ModelAdmin):
    list_display = ('id', 'username', 'first_name', 'last_name', 'email', 'gender')
    list_filter = ('gender',)


class ActivityTypeAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'description')


class ActivityAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'activity_type')


class AnnouncementAdmin(admin.ModelAdmin):
    list_display = ('id', 'creator', 'activity_type', 'activity', 'gender_preference',
                    'skill', 'location')
    list_filter = ('creator', 'skill', 'location')


class MessageAdmin(admin.ModelAdmin):
    list_display = ('sender', 'receiver', 'created')


class DiscussionAdmin(admin.ModelAdmin):
    list_display = ('user1', 'user2', 'created')


class PlaceAdmin(admin.ModelAdmin):
    list_display = ('name',)


admin.site.register(User, UserAdmin)
admin.site.register(ActivityType, ActivityTypeAdmin)
admin.site.register(Activity, ActivityAdmin)
admin.site.register(Announcement, AnnouncementAdmin)
admin.site.register(Message, MessageAdmin)
admin.site.register(Discussion, DiscussionAdmin)
admin.site.register(Place, PlaceAdmin)
