from settings import *

SECRET_KEY = ''

DEBUG = False
TEMPLATE_DEBUG = False
ALLOWED_HOSTS = ('.game-buddy.info',)

STATIC_ROOT = '/var/www/static/gamebuddy-backend'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'gamebuddydb',
        'USER': 'gamebuddy',
        'PASSWORD': '',
        'HOST': 'localhost',
        'PORT': '', # 5432
    },
}

CORS_ORIGIN_ALLOW_ALL = True
