# -*- coding: utf-8 -*-

from rest_framework.views import exception_handler
from rest_framework.exceptions import APIException


class NotFound(APIException):

    status_code = 404
    default_detail = 'Not found.'

    def __init__(self, detail):
        self.detail = detail
