from rest_framework.serializers import ValidationError
from rest_framework import serializers
from rest_framework import exceptions as exc
from django.db.models import Field

from gamebuddy.models import User, ActivityType, Announcement, MyPhoto, Message, Discussion, \
                             Place

class PhotoSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = MyPhoto
        fields = ('id', 'image')


class UserSerializerGET(serializers.ModelSerializer):
    picture = PhotoSerializer()

    class Meta:
        model = User
        fields = ('id', 'username', 'last_name', 'first_name', 'email', 'password',
                  'last_login', 'date_joined', 'picture')
        #write_only_fields = ('password',)
        read_only_fields = ('id', 'last_login', 'date_joined')


class UserSerializerPOST(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('username', 'last_name', 'first_name', 'email', 'password')

    def restore_object(self, attrs, instance=None):
        # call set_password on user object. Without this
        # the password will be stored in plain text.
        user = super(UserSerializerPOST, self).restore_object(attrs, instance)
        user.set_password(attrs['password'])
        return user

class PasswordSerializerPUT(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('username', 'last_name', 'first_name', 'email', 'password', 'picture')

    def restore_object(self, attrs, instance=None):
        # call set_password on user object. Without this
        # the password will be stored in plain text.
        user = super(PasswordSerializerPUT, self).restore_object(attrs, instance)
        user.set_password(attrs['password'])
        print "asdasdasd"
        return user



class ActivityTypeSerializerPOST(serializers.ModelSerializer):

    class Meta:
        model = ActivityType
        fields = ('name', 'description')

class ActivityTypeSerializerGET(serializers.ModelSerializer):

    class Meta:
        model = ActivityType
        fields = ('id', 'name', 'description')


class ActivitySerializerGET(serializers.ModelSerializer):

    activity_type = ActivityTypeSerializerGET()

    class Meta:
        model = ActivityType
        fields = ('id', 'name', 'activity_type')


class PlaceSerializer(serializers.ModelSerializer):

    class Meta:
        model = Place
        fields = ('name', 'description')


class AnnouncementSerializerGET(serializers.ModelSerializer):
    '''
    Serializer for GETting announcements
    '''

    creator = UserSerializerGET()
    activity_type = ActivityTypeSerializerGET()
    coordinates = serializers.Field(source='coordinates')
    skill_level = serializers.Field(source='skill_level')
    location = PlaceSerializer()

    class Meta:
        model = Announcement
        fields = ('id', 'creator', 'activity', 'activity_type', 'description',
                'gender_preference', 'location', 'created',
                'modified', 'coordinates', 'skill_level')
        read_only_fields = ('id', 'created', 'modified')


class AnnouncementSerializerPOST(serializers.ModelSerializer):
    '''
    Serializer for POSTing announcements
    '''

    class Meta:
        model = Announcement
        fields = ('creator', 'activity', 'activity_type', 'description',
                'gender_preference', 'skill', 'location', 'latitude', 'longitude')

class AnnouncementSerializerDELETE(serializers.ModelSerializer):
    '''
    Serializer for DELETEing announcements
    '''

    class Meta:
        model = Announcement
        fields = ('creator', 'activity', 'activity_type', 'description',
                'gender_preference', 'skill', 'location', 'latitude', 'longitude')

class MessagePOST(serializers.ModelSerializer):

    class Meta:
        model = Message
        fields = ('sender', 'receiver', 'content', 'discussion', 'read')

class MessageGET(serializers.ModelSerializer):

    sender = UserSerializerGET()
    receiver = UserSerializerGET()

    class Meta:
        model = Message
        fields = ('id', 'sender', 'receiver', 'content', 'created', 'discussion', 'read')

class DiscussionPOST(serializers.ModelSerializer):

    class Meta:
        model = Discussion
        fields = ('user1', 'user2')


class DiscussionsGET(serializers.ModelSerializer):
    unread_count = serializers.SerializerMethodField('GetCountUnRead')
    messages = serializers.SerializerMethodField('getMessages')

    user1 = UserSerializerGET()
    user2 = UserSerializerGET()
    class Meta:
        model = Discussion
        fields = ('id', 'user1', 'user2','unread_count', 'messages')

    def GetCountUnRead(self, obj):
        user = User.objects.filter(username=obj.user1)[0]
        messages = Message.objects.filter(receiver=user.id).filter(read=False)

        return len(messages)

    def getMessages(self, obj):
        user = User.objects.filter(username=obj.user1)[0]
        messages = Message.objects.filter(receiver=user.id)
        messageJson = MessageGET(messages, many=True)

        return messageJson.data
